<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/font-awesome.min.css',
//        'css/magnific-popup.css',
//        'css/nice-select.css',
//        'css/owl.carousel.css',
//        'css/linearicons.css',
//        'css/main.css',
        'css/site.css',
//        'css/bootstrap.css',
//        'css/font-awesome.min.css',
//        'css/magnific-popup.css',
//        'css/nice-select.css',
//        'css/owl.carousel.css',
//        'css/linearicons.css',
//        'css/main.css',
    ];
    public $js = [

        'js/site.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
