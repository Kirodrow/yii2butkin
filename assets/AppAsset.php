<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        'css/bootstrap.css',
        'css/font-awesome.min.css',
        'css/magnific-popup.css',
        'css/nice-select.css',
        'css/owl.carousel.css',
        'css/linearicons.css',
        'css/main.css',
        'css/site.css',
    ];
    public $js = [
        'js/bootstrap.js',
        'js/jquery.ajaxchimp.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/jquery.nice-select.min.js',
        'js/main.js',
        'js/owl.carousel.min.js',
        'js/site.js',
        'js/jquery.maskedinput.min.js',
//

    ];

    public $depends = [
        'yii\web\YiiAsset',
//        'app\assets\BootstrapAsset',
    ];
}
