<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meta".
 *
 * @property int $id
 * @property string $type
 * @property string $news_id
 * @property string $title
 * @property string $keywords
 * @property string $description
 */
class Meta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'safe'],
            [['title', 'keywords', 'description'], 'safe'],
            [['type', 'news_id'], 'safe'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'news_id' => 'Значение мета атрибута(страница)',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
        ];
    }

    public function saveMeta($id, $request)
    {
        $meta = Meta::find()->where(['type' => $id])->one();
        if (!$meta) {
            $meta = new Meta();
        }
        $meta->type = "$id";
        $meta->news_id = $request['Post']['title'];
        $meta->title = $request['Meta']['title'];
        $meta->keywords = $request['Meta']['keywords'];
        $meta->description = $request['Meta']['description'];
        $meta->save();

    }

    public function standartMeta(){
        $languange = Yii::$app->language;

        $meta = new Meta();
        if($languange == 'ru'){
            $meta->title = 'Новость/Галлерея';
            $meta->description = 'Новость/Галлерея';
            $meta->keywords = 'Новость/Галлерея';
            return $meta;
        }

        if($languange == 'kz'){
            $meta->title = 'Новость/ГаллереяKz';
            $meta->description = 'Новость/ГаллереяKZ';
            $meta->keywords = 'Новость/ГаллереяKZ';
            return $meta;
        }


    }
}
