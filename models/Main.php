<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "main".
 *
 * @property int $id
 * @property string $img
 * @property string $video
 * @property string $short_text
 * @property string $full_text
 * @property string $lang
 */
class Main extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['video', 'full_text', 'short_text', 'lang'], 'string'],
            [['video', 'full_text', 'short_text', 'lang'], 'required'],
//            [['video, full_text, short_text, lang'], 'required' ],
            [['img'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Картинка',
            'lang' => 'Язык',
            'video' => 'Video',
            'short_text' => 'Краткий текст',
            'full_text' => 'Полный текст',
        ];
    }
}
