<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string $alt
 * @property int $type
 * @property int $post_id
 * @property string $img
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'post_id'], 'integer'],
            [['img'], 'required'],
            [['img'], 'string', 'max' => 255],
            [['img'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alt' => 'Alt',
            'type' => 'Type',
            'post_id' => 'Post ID',
            'img' => 'Img',
        ];
    }
}
