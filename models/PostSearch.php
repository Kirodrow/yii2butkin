<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Post;

/**
 * PostSearch represents the model behind the search form of `app\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id',  'activity'], 'integer'],
            [['short_text', 'title', 'date', 'type','full_text', 'short_img', 'full_img', 'city'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = false)
    {
        if($type != false){
            $query = Post::find()->where(['type' => $type])->orderBy([
                'date' => SORT_DESC //Need this line to be fixed
            ]);
        }
        else{
            $query = Post::find()->where(['type' => 0])->orderBy([
                'date' => SORT_DESC //Need this line to be fixed
            ]);;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'title' => $this->title,
            'date' => $this->date,
            'type' => $this->type,
            'activity' => $this->activity,
        ]);

        $query->andFilterWhere(['like', 'full_text', $this->full_text])
            ->andFilterWhere(['like', 'short_img', $this->short_img])
            ->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }
}
