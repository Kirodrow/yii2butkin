<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $title
 * @property string $full_text
 * @property string $short_img
 * @property int $date
 * @property int $gallery
 * @property int $type
 * @property string $city
 * @property int $activity
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title',  'lang', 'full_text', 'type',], 'required'],
            [[  'type', 'activity'], 'integer'],
            [['full_text', 'short_img'], 'string'],
            [['city'], 'string', 'max' => 255],
            [['gallery'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'full_text' => 'Текст',
            'short_img' => 'Short Img',
            'full_img' => 'Full Img',
            'date' => 'Date',
            'type' => 'Type',
            'city' => 'City',
            'activity' => 'Activity',
        ];
    }
}
