<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registration".
 *
 * @property int $id
 * @property string $name
 * @property string $birthday
 * @property string $study
 * @property string $city
 * @property string $email
 * @property string $phone
 * @property int $date
 * @property string $file
 */
class Registration extends \yii\db\ActiveRecord
{
    public $checkbox;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['name'], 'required', 'message' => Yii::t('common', 'message_fio')],
            [['birthday'], 'required', 'message' => Yii::t('common', 'message_date')],
            [['study'], 'required', 'message' => Yii::t('common', 'message_study')],
            [['city'], 'required', 'message' => Yii::t('common', 'message_city')],
            [['email'], 'required', 'message' => Yii::t('common', 'message_email')],
            [['phone'], 'required', 'message' => Yii::t('common', 'message_phone')],
            ['checkbox', 'required', 'requiredValue' => 1, 'message' => Yii::t('common', 'Подвердите ознакомление с правилами участия')],
            [ ['file'], 'file',  'maxSize' => 50*(1024*1024),    //10MB
                'tooBig' => Yii::t('common', 'so_big')],
            [['study', 'city', 'email', 'phone'], 'string'],
            [['email'], 'email', 'message' => Yii::t('common', Yii::t('common', 'message_error_email'))],
            [['date'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'checkbox' => 'IsdfsdfsdD',
            'id' => 'ID',
            'name' => 'имя',
            'birthday' => Yii::t('common', 'date_of_birth'),
            'study' => 'место обучения',
            'city' => 'город',
            'email' => 'Email',
            'phone' => 'телефон',
            'date' => 'Date',
            'file' => 'File',
        ];
    }
    public function upload()
    {
        if ($this->validate()) {

            $this->file->saveAs('img/registration/' . time() . '.' . $this->file->extension);
            $this->file =  time() . '.' . $this->file->extension;
            return true;
        } else {
            return false;
        }
    }
    function getExtension($filename) {
        $path_info = pathinfo($filename);
        return $path_info['extension'];
    }


}
