<?php

namespace app\controllers;

use Yii;
use app\models\Registration;
use app\models\RegistrationSearch;
use yii\base\Module;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;



/**
 * RegistrationController implements the CRUD actions for Registration model.
 */
class RegistrationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        Yii::$app->language = 'ru';

    }

    /**
     * Lists all Registration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegistrationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Registration model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model =  $this->findModel($id);
       $extension =  $model->getExtension($model->file);
       $allExt = ['png', 'jpg', 'jpeg'];
        if(in_array($extension, $allExt)){
            $img = [
                'attribute'=>'file',
                'value' =>'/img/registration/' .$model->file,
                'format' =>['image',['width'=>'200','height'=>'200'],

                ],

            ];
        }

        else{
            $img = 'file';
        }
        if(!empty($model->file)){
        $download =  [
                'attribute'=>'file',
                'label'=>'Скачать файл',
                'format'=>'raw',
                'value'=>Html::a( 'Скачать файл', Url::toRoute(['/registration/download', 'alias' => $model->file])),
            ];
        }
        else $download =  [
            'attribute'=>'file',
            'label'=>'Скачать файл',
            'format'=>'raw',
            'value'=> 'Файл отсутствует',
            ];


        return $this->render('view', [
            'model' => $model,
            'img' => $img,
            'download' => $download
        ]);
    }

    /**
     * Creates a new Registration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Registration();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Registration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }




    /**
     * Deletes an existing Registration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Registration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Registration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Registration::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDownload()
    {
        $alias = $_REQUEST['alias'];
        $file = Yii::getAlias('@app/web/img/registration/'.$alias);
        return Yii::$app->response->sendFile($file);

    }
}
