<?php

namespace app\controllers;

use app\models\Main;
use app\models\Meta;
use app\models\Post;
use app\models\Questions;
use app\models\Registration;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;


class SiteController extends Controller
{
    public $layout = 'site';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {


        return [

            'page' => [
                'class' => \yii\web\ViewAction::className(),
                'viewPrefix' => 'pages/' . \Yii::$app->language,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $registration = new Registration();
        if ($registration->load(Yii::$app->request->post()) && $registration->validate()) {

            $registration->date = time();
            $registration->file = UploadedFile::getInstance($registration, 'file');
            if (!empty($registration->file)) {

                $registration->upload();


            }
            $registration->save();
            $registration = new Registration();

            return $this->redirect('/#submit-application');
        }
        $meta = Meta::find()->where(['type' => 'main'])->one();
        $type = 0;
        $titlePage = $this->titlePage($type);
        $languange = Yii::$app->language;
        $content = Main::find()->where(['lang' => $languange])->one();
//        pr($content);
        $post = Post::find()->where(['type' => $type, 'lang' => $languange, 'activity' => '1'])->orderBy([
            'date' => SORT_DESC])->one();
//        pr($post);
        $staticPage = $this->renderPartial('page', compact('content'));



        return $this->render('index', compact('meta','content','registration',  'titlePage', 'post', 'staticPage'));
    }


    public function actionProject()
    {

        $languange = Yii::$app->language;
        $model = Post::find()->where(['type' => 2, 'lang' => $languange, 'activity' => '1'])->one();
        $meta = Meta::find()->where(['type' => $model->id])->one();

        return $this->render('project', compact('model', 'meta'));
    }

    public function actionParticipants()
    {

        $languange = Yii::$app->language;
        $model = Post::find()->where(['type' => 5, 'lang' => $languange, 'activity' => '1'])->one();
        $meta = Meta::find()->where(['type' => $model->id])->one();
        return $this->render('participants', compact('model', 'meta'));
    }

    public function actionCondition()
    {

        $languange = Yii::$app->language;
        $model = Post::find()->where(['type' => 3, 'lang' => $languange, 'activity' => '1'])->one();
        $meta = Meta::find()->where(['type' => $model->id])->one();
        return $this->render('condition', compact('model', 'meta'));
    }


    public function actionWinners()
    {

        $languange = Yii::$app->language;
        $model = Post::find()->where(['type' => 4, 'lang' => $languange, 'activity' => '1'])->one();
        $meta = Meta::find()->where(['type' => $model->id])->one();

//        pr($model);
        return $this->render('winners', compact('model', 'meta'));
    }

    protected function titlePage($type)
    {

        $type == 0 ? $title = 'Архив новостей' : $title = 'Галлерея';
        return $title;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionList()
    {
        $posts = Registration::find();
        $pages = new Pagination(['totalCount' => $posts->count(), 'pageSize' =>20]);
        $list = $posts->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('list', compact('list', 'pages'));

    }

    public function actionForm()
    {
        $registration = new Registration();


        if ($registration->load(Yii::$app->request->post())) {

            $registration->date = time();
            $registration->file = 'ddasa';
//            $registration->city =$_REQUEST['city'];

            $fileName = 'file';
            $uploadPath = Yii::$app->basePath . '/web/files/';
//            echo '<pre>';
//
//            print_r($_REQUEST);
//            die;
            if (isset($_FILES[$fileName])) {

                $file = \yii\web\UploadedFile::getInstanceByName($fileName);

                $name = time() . '.' . $file->extension;

                if ($file->saveAs($uploadPath . '/' . $name)) {

                    $registration->file = $name;
                }
            }
            $registration->save();
        }
        return $this->render('form', compact('registration'));
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionQuestion(){



        $languange = Yii::$app->language;
        $content = Questions::find() ->where(['lang' => $languange])->all();
//
        return $this->render('question', compact('content'));
    }
}
