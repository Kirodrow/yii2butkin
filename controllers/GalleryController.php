<?php

namespace app\controllers;

use app\models\Image;
use app\models\Meta;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use DataTime;

class GalleryController extends Controller
{
    public $layout = 'site';


    public function actionIndex(){
//        $meta = Meta::find()->where(['type' => 'news'])->one();

        $language = Yii::$app->language;
        $type = $_REQUEST['type'];

        $meta = $this->meta($type, $language);
        $title = $this->title($type);


        $posts = Post::find()->where(['type' => $type, 'lang' => $language, 'activity' => 1])->orderBy([
            'date' => SORT_DESC]);
        $pages = new Pagination(['totalCount' => $posts->count(), 'pageSize' =>6]);
        $post = $posts->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', compact('meta','post', 'title', 'pages'));
    }

    public function meta($type, $language){

        if($type == 0 ){
            $meta['title'] = Yii::t('common', 'TitleNews');
            $meta['keywords'] = Yii::t('common', 'keywordsNews');
            $meta['description'] = Yii::t('common', 'descriptionNews');


        }

        if($type == 1){

            $meta['title'] = Yii::t('common', 'TitleGallery');
            $meta['keywords'] = Yii::t('common', 'keywordsGallery');
            $meta['description'] = Yii::t('common', 'descriptionGallery');

        }
        return $meta;
    }
    public function actionShow(){

        $id = $_REQUEST['id'];
        $post = Post::find()->where(['id' => $id])->one();
        $meta = Meta::find()->where(['type' => $id])->one();

        if(!$meta){
            $meta = Meta::standartMeta();
        }
        $date = date($post->date);
        $language = Yii::$app->language;

        $date = dateFormat($language, $date);
        if($post->type == 0){
            $gallery = Post::find()->where(['id' => $post->gallery])->one();
            return $this->render('news', compact('meta', 'post', 'date', 'gallery'));
        }
        else{
            $query = Image::find()->where(['post_id' => $id, 'type' => $post->type]);
            $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);
            $post = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('view', compact('meta', 'post', 'pages'));
        }

    }


    public function actionView(){

        $id = $_REQUEST['id'];
        $meta = Meta::find()->where(['type' => $id])->one();

        if(!$meta){
            $meta = Meta::standartMeta();
        }
        $type = $_REQUEST['type'];
        $query = Image::find()->where(['post_id' => $id, 'type' => $type]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);
        $post = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('view', compact('meta','post', 'pages'));
    }


    protected function title($type){

        $type == 0 ? $title = Yii::t('common', 'news-title') : $title =Yii::t('common', 'gallery_title');
        return $title;
    }
}