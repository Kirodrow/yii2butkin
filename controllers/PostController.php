<?php

namespace app\controllers;

use app\models\Meta;
use Imagine\Image\Box;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\base\Module;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public $full_image;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        // Yii::$app->language = 'ru';

    }

    public function init()
    {
        parent::init();
        //  Yii::$app->language = 'ru';
        #add your logic: read the cookie and then set the language
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionNews()
    {

        $session = Yii::$app->session;
        $session->open();
        $session['page'] = 'news';


        $title = 'Список новостей';
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 0);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title
        ]);
    }

    public function actionWinners()
    {
        $session = Yii::$app->session;
        $session->open();
        $session['page'] = 'winners';


        $title = 'Список Победителей';
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 4);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title
        ]);
    }

    public function actionParticipants()
    {

        $session = Yii::$app->session;
        $session->open();
        $session['page'] = 'participants';

        $title = 'Список Участников';
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 5);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title
        ]);
    }

    public function actionGallery()
    {
        $session = Yii::$app->session;
        $session->open();
        $session['page'] = 'gallery';

        $title = 'Список галерей';
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 1);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();
        $meta = new  Meta();
        $gallery = Post::find()->where(['type' => 1])->all();

        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session;
            $session->open();
            if(isset(Yii::$app->request->post('Post')['date'])){
                $model->date = strtotime(Yii::$app->request->post('Post')['date']);
            }
            else{ $model->date = time();}

            if (isset($session['short_img'])) $model->short_img = $session['short_img'];

//            if (isset($session['full_img'])) $model->full_img = $session['full_img'];
            if ($model->save()) {

//

                $meta->saveMeta($model->id, $_REQUEST);
                unset($session['short_img']);
//                unset($session['full_img']);
                if($model->type == 1    ){

                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model, 'meta' => $meta, 'gallery' => $gallery
        ]);
    }


    public function actionProject()
    {
        $title = 'О проекте';
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 2);
        $session = Yii::$app->session;
        $session->open();
        $session['page'] = 'project';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title
        ]);

    }


    public function actionCondition()
    {

        $title = 'Условия участия';
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 3);
        $session = Yii::$app->session;
        $session->open();
        $session['page'] = 'condition';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title
        ]);
    }


    public function actionConditions()
    {

        $model = Post::find()->where(['type' => 3])->one();
        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session;
            $session->open();
            $model->date = time();

            if (isset($session['short_img']) && $model->short_img != '') {
                if (file_exists('img/post/' . $model->short_img)) {
                    $handle = fopen('img/post/' . $model->short_img, 'r');
                    fclose($handle);
                    unlink('img/post/' . $model->short_img);

                }

                $model->short_img = $session['short_img'];
            }
            if ($model->save()) {
                unset($session['short_img']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('project', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $meta = Meta::find()->where(['type' => $id])->one();
        if (!$meta) {
            $meta = new Meta();
        }
        $gallery = Post::find()->where(['type' => 1])->all();

        $model = $this->findModel($id);
//                print_r($model);


        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session;
            $session->open();
            $model->date = strtotime(Yii::$app->request->post('Post')['date']);

            if (isset($session['short_img']) && $model->short_img != '') {
                if (file_exists('img/post/' . $model->short_img)) {
                    $handle = fopen('img/post/' . $model->short_img, 'r');
                    fclose($handle);
                    unlink('img/post/' . $model->short_img);
                }

            }
            if (isset($session['short_img'])) {
                $model->short_img = $session['short_img'];
            }

//            if (isset($session['full_img']) && $model->full_img != '') {
//                if (file_exists('img/post/' . $model->full_img)) {
//
//                    $handle = fopen('img/post/' . $model->full_img, 'r');
//                    fclose($handle);
//                    unlink('img/post/' . $model->full_img);
//
//                }
//
//                $model->full_img = $session['full_img'];
//            }

            if ($model->save()) {
                $meta->saveMeta($id, $_REQUEST);
                unset($session['short_img']);
//                unset($session['full_img']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', ['model' => $model, 'meta' => $meta, 'gallery' => $gallery]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    { $session = Yii::$app->session;
        $session->open();


        $this->findModel($id)->delete();

        return $this->redirect('/post/'.$session['page']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionUpload()
    {

        $fileName = 'file';
        $uploadPath = Yii::$app->basePath . '/web/img/post';
        if (isset($_FILES[$fileName])) {

            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            $name = time() . '.' . $file->extension;

            if ($file->saveAs($uploadPath . '/' . $name)) {


                $img = Image::getImagine()->open(Yii::getAlias($uploadPath . '/' . $name));

                $size = $img->getSize();
                $ratio = $size->getWidth() / $size->getHeight();


                $widthMini = 600;
                $heightMini = round($widthMini / $ratio);
                $boxMini = new Box($widthMini, $heightMini);

                $img->resize($boxMini)->save($uploadPath . '/' . 'mini_' . $name);

                $session = Yii::$app->session;
                $session->open();
                $session['short_img'] = $name;

            }
        }


        return false;
    }

//
//    public function actionFull()
//    {
//
//        $fileName = 'file';
//        $uploadPath = Yii::$app->basePath . '/web/img/post';
//        if (isset($_FILES[$fileName])) {
//
//            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
//
//
//            $name = time() . '.' . $file->extension;
//
//            if ($file->saveAs($uploadPath . '/' . $name)) {
//                $session = Yii::$app->session;
//                $session->open();
//                $session['full_img'] = $name;
//            }
//        }
//
//        return false;
//    }
}
