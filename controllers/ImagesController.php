<?php
namespace app\controllers;

use app\models\Image;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class ImagesController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex(){
        $id = $_REQUEST['id'];
        $type = $_REQUEST['type'];
        $modelPost = new Post();
        $modelPost = $modelPost->find()->where(['id' => $id, 'type' => $type])->one();
        $model = new Image();
        $allImages = $model->find()->where(['post_id' => $id, 'type' => $type])->asArray()->all();
        return $this->render('index', compact('type', 'id', 'allImages', 'modelPost'));
        }


    public function actionDel(){

        $id = $_REQUEST['id'];


        $customer = Image::find()->where(['id' => $id])->one();
        unlink('img/allImages/'.$customer->img);
        $customer->delete();
        return ;
    }

    public function actionUpload()
    {



        $fileName = 'file';
        $uploadPath = Yii::$app->basePath.'/web/img/allImages';
        if (isset($_FILES[$fileName])) {

            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            $name = time().'.'.$file->extension;

            if ($file->saveAs($uploadPath . '/' . $name)) {
                $model = new Image();
                $model->post_id = $_REQUEST['id'];
                $model->type = $_REQUEST['type'];
                $model->img = $name;
                $model->save();
            }
        }

        return false;
    }

}