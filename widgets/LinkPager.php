<?php

namespace app\widgets;


use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class LinkPager extends \justinvoelker\separatedpager\LinkPager
{


    public $separator = '...';
    protected function renderPageButton($label, $page, $class, $disabled, $active)
    {

        $options = $this->linkContainerOptions;
        $linkWrapTag = ArrayHelper::remove($options, 'tag', 'li');
        Html::addCssClass($options, empty($class) ? $this->pageCssClass : $class);

        if ($active) {
            Html::addCssClass($options, $this->activePageCssClass);
        }
        if ($disabled) {
            Html::addCssClass($options, $this->disabledPageCssClass);

            return Html::tag('li', Html::tag('span', $label), $options);
        }
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        // active page as anchor or span
        if ($active && !$this->activePageAsLink) {
            return Html::tag('li', Html::tag('span', $label, $linkOptions), $options);
        }

        return Html::tag('li', Html::a($label, $this->pagination->createUrl($page), $linkOptions), $options);
    }


}