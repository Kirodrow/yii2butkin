<?php

return [
    'main_title1' => 'Начало приема заявок',
    'main_title2' => 'на 3-й сезон проекта',
    'main_title3' => '«3.2.1.Старт!»',
    'main_button' => 'Заполнить форму',
    'about_project_title' => 'Кратко о проекте',
    'more_read' => 'Читать далее',
    'condition_title' => 'Условия участия',
    'gallery_title' => 'Галерея',
    'submit-application' => 'Отправить заявку',
    'about-project' => 'О проекте',
    'winners' => 'Победители',
    'apply-application' => 'Подать заявку',
    'news-title' => 'Новости',
    'questions-and-answers' => 'Вопросы и ответы',
    'contacts-title' => 'Контакты',
    'news-archive' => 'Архив новостей',
    'photo_title' => 'Фотографии',
    'album_title' => 'Альбомы',
    'main' => 'Главная',
    'gallery' => 'Галерея kz',
    'list_participants' => 'Список участников',
    'participants' =>'Участники',
    'fio' => 'ФИО',
    'date_of_birth' => 'Дата рождения',
    'city' => 'Город',
    'place_study' => 'Место обучения',
    'phone' => 'Номер телефона',
    'attach_file' => 'Прикрепить файл',
    'attach_file_size' => 'Не более 50 МБ',
    'condition_accept' => 'Я согласен с <a href="/site/condition" data-pjax = "0" target="_blank">условиями участия</a>',
    'message_fio' => 'Пожалуйста, заполните поле "ФИО"',
    'message_date' => 'Пожалуйста, заполните поле "Дата рождения"',
    'message_city' => 'Пожалуйста, заполните поле "Город"',
    'message_study' => 'Пожалуйста, заполните поле "Место обучения"',
    'message_email' => 'Пожалуйста, заполните поле "E-mail"',
    'message_phone' => 'Пожалуйста, заполните поле "Номер телефона"',
    'message_error_email' => 'Пожалуйста, введите корректный e-mail',
    'TitleNews' => 'Новости',
    'keywordsNews' => 'новости coca-cola',
    'descriptionNews' => 'Новости проекта 321start',
    'TitleGallery' => 'Галерея',
    'keywordsGallery' => 'Галерея 321start',
    'descriptionGallery' => 'Галерея проекта 321start',
    'TitleQuestions' => 'Вопросы и ответы',
    'keywordsQuestions' => 'Вопросы и ответы 321start',
    'descriptionQuestions' => 'Вопросы и ответы проекта 321start',
    'message_condition' => 'Подвердите ознакомление с правилами участия',
    'title-application' => 'Заявка на участие',
    'send-application' => 'Отправить',
    'questions-text' => 'Здесь вы найдете ответы на наиболее часто задаваемые вопросы, касающиеся проекта «3.2.1. Старт!».  Если у вас есть вопрос, на который вы не нашли ответ на этой странице, напишите его нам: <a href="mailto:hotline@cci.com.kz" rel="nofollow" target="_blank">hotline@cci.com.kz</a> 
'
];