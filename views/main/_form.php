<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Main */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
       изображение
        <?php if($model->img != ''){?>
            <div>
                <?= Html::img('/img/main/'.$model->img,["width"=>"200px"]);?>
            </div>
        <?php }?>
        <?php echo \kato\DropZone::widget([
            'id'        => 'dzFile', // <-- уникальные id
            'uploadUrl' => Url::toRoute([ '/main/upload' ]),
            'dropzoneContainer' => 'dz-container-files', // <-- уникальные dropzoneContainer
            'previewsContainer' => 'preview-files', // <-- уникальные previewsContainer
            'options' => [
//                    'maxFilesize' => '2',
            ],
            'clientEvents' => [
                'complete' => "function(file){console.log(file)}",
                'removedfile' => "function(file){alert(file.name + ' is removed')}"
            ],
        ]);?>
    </div>

    <?= $form->field($model, 'video')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'full_text')->textarea(['rows' => 6]) ?>
    <?php echo $form->field($model, 'lang')->label('Язык')->dropDownList([
        'ru' => 'ru',
        'kz' => 'kz',

    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
