<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use kato\DropZone;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>

Загружать по 1-2 фотографии

<?php Pjax::begin(); ?>
<div class="imagesMain">
    <?php foreach($allImages as $img){?>
        <div class="images" id="img<?=$img['id']?>" >
            <?= Html::img('/img/allImages/'.$img['img'],["width"=>"200px"]);?>
            <div class="sel" ><a class="sel" onclick="deleteImage(<?=$img['id']?>) ">x</a></div>
        </div>


    <?php }?>
</div>

<div>
    <?= \kato\DropZone::widget([
        'id'        => 'dzImage', // <-- уникальные id
        'uploadUrl' => Url::toRoute([ '/images/upload', 'id' => $id, 'type' => $type ]),
        'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
        'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
        'options' => [
            'maxFiles' => '1',

        ],
        'clientEvents' => [
            'complete' => "function(file){console.log(file)}",
            'removedfile' => "function(file){alert(file.name + ' is removed')}"
        ],
    ]);?>
</div>

<?php Pjax::end();?>