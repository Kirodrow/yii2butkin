<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\LinkPager;
$this->title = $meta['title'];
$this->registerMetaTag(['name' => 'keywords', 'content' => $meta['keywords']]);
$this->registerMetaTag(['name' => 'description', 'content' => $meta['description']]);
?>

<div class="page-news-block-shape-left <? if (($_GET['type'])== 1): ?>page-news-block-shape-yellow<? endif; ?>"></div>
<div class="page-news-block-shape-right <? if (($_GET['type'])== 1): ?>page-news-block-shape-blue<? endif; ?>"></div>

<section class="page-news-area">
    <div class="container">
        <h1 class="text-heading"><?=$title?></h1>
        <div class="page-content-allnews">
            <div class="row">
                <?php foreach ($post as $value){?>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <a class="single-main-news" href="/gallery/show?id=<?php echo $value->id?>">
                        <span class="block-image-news" style="background-image: url('/img/post/mini_<?php echo $value->short_img?>');"></span>
                        <span class="block-title-service">
                            <?= date('d.m.Y',$value->date);?>
                            <span class="title-main-news"><?= $value->title ?></span>
                        </span>
                    </a>
                </div>
                <?php }?>
            </div>
        </div>

        <div class="pagination_gallery">
            <?php echo LinkPager::widget([
                    'pagination' => $pages,
                    'pageCssClass' => 'pageClass',
                    'activePageCssClass' => 'currentPage',
                    'maxButtonCount' => 7,
                    'prevPageLabel' => '&lsaquo;',
                    'nextPageLabel' => '&rsaquo;',
                    'prevPageCssClass' => 'prev pageClass',
                    'nextPageCssClass' => 'next pageClass',
                    'activePageAsLink' => false,
                    'disableCurrentPageButton' => true,
                    'hideOnSinglePage' => true,
            ]); ?>
        </div>
    </div>
</section>



