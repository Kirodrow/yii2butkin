<?php
/**
 * Created by PhpStorm.
 * User: Kirod
 * Date: 11.12.2018
 * Time: 19:45
 */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $meta->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => "$meta->keywords"]);
$this->registerMetaTag(['name' => 'description', 'content' => $meta->description]);

?>

<div class="page-image-block page-news-image-block">
    <?= Html::img('/img/main/top-image.png'); ?>
</div>

<div class="page-block-yellow-shape"></div>
<div class="page-block-blue-shape"></div>
<div class="container container-page">
    <div class="container-page-wrapper">
        <h1 class="text-left title-page-txt"><?= ucfirst($post->title) ?></h1>
        <div class="block-data-news">
            <span class="block-news-city"><?= ucfirst($post->city) ?></span>
            <?= $date ?>
        </div>
        <div class="block-fulltxt-page">
            <p>
                <?= Html::img('/img/post/' . $post->short_img, ['class' => "img-fluid"]); ?>
            </p>
            <?= $post->full_text ?>
        </div>

        <?php if($gallery){?>
            <div class="block-news-gallery">
                <?= Html::img('/img/post/' . $gallery->short_img, ['class' => "img-fluid"]); ?>
                <?= Html::a('Перейти в галлерею', Url::toRoute(['/gallery/view', 'id' => $gallery->id, 'type' => $gallery->type]), ['class' => 'genric-btn more danger']); ?>
            </div>
        <?php }?>
    </div>
</div>