<?php

use app\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $meta->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => "$meta->keywords"]);
$this->registerMetaTag(['name' => 'description', 'content' => $meta->description]);

?>


<section class="page-news-area">
    <div class="container">
        <h1 class="text-heading"><?=Yii::t('common', 'gallery_title')?></h1>
        <div class="breadcrumb-block">
            <?=Html::a(Yii::t('common', 'album_title'), Url::toRoute(['/gallery/index', 'type' => '1']));?>
            <span class="breadcrumb-separator">/</span>
            <span><?=Yii::t('common', 'photo_title')?></span>
        </div>
        <div class="page-content-allimages">
            <div class="row">
                <?php foreach ($post as $img){?>
                    <div class="col-lg-4 col-sm-6 col-xs-6">
                        <a href="/img/allImages/<?=$img->img?>" class="img-pop-up"><span class="single-gallery-image" style="background: url(/img/allImages/<?=$img->img?>)"></span></a>
                    </div>
                <?php }?>
            </div>
        </div>

        <div class="pagination_gallery">
            <?php echo LinkPager::widget([
                    'pagination' => $pages,
                    'pageCssClass' => 'pageClass',
                    'activePageCssClass' => 'currentPage',
                    'maxButtonCount' => 7,
                    'prevPageLabel' => '&lsaquo;',
                    'nextPageLabel' => '&rsaquo;',
                    'prevPageCssClass' => 'prev pageClass',
                    'nextPageCssClass' => 'next pageClass',
                    'activePageAsLink' => false,
                    'disableCurrentPageButton' => true,
                    'hideOnSinglePage' => true,
            ]); ?>
        </div>
    </div>
</section>
