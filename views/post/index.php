<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
 <div class="post-index">

    <h1><?= $title ?></h1>


    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'width: 205px; max-width:205px; '],
         'layout'=>"{summary}\n{items}",
//
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],



            [
                'attribute'=>'title',
                'contentOptions' => ['style' => 'overflow-y: scroll;  
                 max-width: 400px;
                 max-height: 400px;
                 width: 100%;
                 height: 100%;'],
            ],


            'short_img:ntext',
            'lang:ntext',
            [

                'attribute' => 'date',

                'format' => ['date', 'd.M.y']

            ],
            'city',
            'activity',

            [
                'class' => \yii\grid\ActionColumn::className(),
                // you may configure additional properties here
            ],
        ],
    ]); ?>

    <div class="col-md-12 ">
        <div class="row margin20 ">

            <div class="pagination_gallery">

                <?php echo \app\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->pagination,

                    'pageCssClass' => 'pageClass',
                    'activePageCssClass' => 'pageClass currentPage',
                    'maxButtonCount' => 7,
                    'prevPageLabel' => '&lsaquo;',

                    'nextPageLabel' => '&rsaquo;',
                    'prevPageCssClass' => 'prev',
                    'nextPageCssClass' => 'next',

                    'activePageAsLink' => false,
                    'disableCurrentPageButton' => true,
                    'hideOnSinglePage' => true,
                ]); ?>
            </div>
        </div>
    </div>
</div>
