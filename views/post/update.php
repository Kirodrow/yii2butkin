<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = 'Обновление новости или галлереи: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="post-update">
    <?php if($model->type == 1){?>
    <?=Html::a('Загрузка фотографий', Url::toRoute(['/images/index', 'id' =>$model->id, 'type' => $model->type]));?>
    <?php }?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'meta' => $meta,
        'gallery' => $gallery

    ]) ?>

</div>
