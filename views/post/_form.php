<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use kato\DropZone;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(

            ['options' =>['enctype' =>'multipart/form-data'],
                'id' => 'post_id',
           ]
    ); ?>


    <?php echo $form->field($model, 'type')->label('Вид записи')->dropDownList([
        '0' => 'Новость',
        '1' => 'Галлерея',
        '2' => 'О проекте',
        '3' => 'Условия участия',
        '4' => 'Страница победителей',
        '5' => 'Страница участников',

    ],
            [
                'id' => 'type',
                'onchange' => 'changeSelect()',
            ]

    );?>

    <?php if($model->type ==0){
        $params = [
            'id' => 'gallery',
            'prompt' => 'Укажите галлерею для новости',
            'style' => 'display: block'
        ];
    }
    else{
        $params = [
            'id' => 'gallery',
            'style' => 'display: none'
        ];
    }
    ?>
    <?php  $items = ArrayHelper::map($gallery,'id','title');

    ?>
    <?=  $form->field($model, 'gallery')->dropDownList($items,$params)->label('');?>
    <?php echo $form->field($model, 'title')->textInput()->label('Заголовок страницы') ?>




    <?php echo  $form->field($model, 'full_text')->label('Полный текст')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]);?>

    <div class="row">
        <div class="col-xs-12" >
            Изображение в списке
            <?php if($model->short_img != ''){?>
            <div>
                <?= Html::img('/img/post/'.$model->short_img,["width"=>"200px"]);?>
            </div>
            <?php }?>


            <?php echo \kato\DropZone::widget([
                'id'        => 'dzImage', // <-- уникальные id
                'uploadUrl' => Url::toRoute([ '/post/upload', 'id' => $model->id,'type' => '0' ]),
                'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                'options' => [
                    'maxFilesize' => '10',
                ],
                'clientEvents' => [
                    'complete' => "function(file){console.log(file)}",
                    'removedfile' => "function(file){alert(file.name + ' is removed')}"
                ],
            ]);?>
        </div>

    </div>





    <?= $form->field($model, 'city')->label('Город')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'lang')->label('Язык')->dropDownList([
        'ru' => 'ru',
        'kz' => 'kz',

    ]);?>

    <?php echo $form->field($model, 'activity')->label('Статус')->dropDownList([
        '1' => 'Опубликованно',
        '0' => 'Отключено',

    ]);?>

    <?= $form->field($model, 'date')->label('Дата(необязательно)')->widget(\yii\jui\DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
        'value'  => $model->date,
        'options' => ['class' => 'date'],
    ]) ?>



    <?php echo  $form->field($meta, 'title')->textInput();?>
    <?php echo  $form->field($meta, 'keywords')->textInput();?>
    <?php echo  $form->field($meta, 'description')->textInput();?>

    <div class="form-group">
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
