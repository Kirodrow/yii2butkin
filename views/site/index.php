<?php

/* @var $this yii\web\View */


//use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\bootstrap4\Modal;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Url;
$this->title = $meta->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => "$meta->keywords"]);
$this->registerMetaTag(['name' => 'description', 'content' => $meta->description]);

?>


<div class="banner-area-main" style="background-image: url('/img/main/<?php echo $content->img?>');">
    <div class="banner__video">
        <video id="banner-video" autoplay="autoplay" loop="loop" muted="muted" style="opacity: 0;">
            <source src="/video/video.mp4" type="video/mp4">
            <source src="/video/video.WebM" type="video/webm">
        </video>
    </div>
    <div class="container">
        <div class="banner-content text-left">
            <h1>
                <span><?= Yii::t('common', 'main_title1') ?></span><br>
                <span><?= Yii::t('common', 'main_title2') ?></span><br>
                <span><?= Yii::t('common', 'main_title3') ?></span>
            </h1>
            <a href="#submit-application" class="genric-btn more danger link-go-block"><?= Yii::t('common', 'main_button') ?></a>
        </div>
        <a class="go-featured-area link-go-block" href="#about-project"></a>
    </div>
</div>


<section class="about-project" id="about-project">
    <h2 class="text-heading text-center"><?= Yii::t('common', 'about_project_title') ?></h2>
    <div class="sample-text">
        <?= $staticPage ?>
    </div>
</section>

<!-- End Feature Area -->
<!-- Start Service Area -->
<section class="main-service-area">
    <div class="container">
        <div class="row main-service-area-content">
            <div class="col-lg-6 col-xs-12">
                <a class="single-main-news" href="/gallery/show?id=<?php echo $post->id?>">
                    <span class="block-image-news" style="background-image: url('/img/post/<?php echo  $post->short_img?>');"></span>
                    <span class="block-title-service">
                        <?= date('d.m.Y',$post->date);?>
                        <span class="title-main-news"><?= $post->title ?></span>
                        <span class="arrow-block-news"></span>
                    </span>
                </a>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="/site/condition" title="<?= Yii::t('common', 'condition_title') ?>" alt="<?= Yii::t('common', 'condition_title') ?>" class="main-service-link main-service-link-condition">
                            <span class="main-service-link-title"><?= Yii::t('common', 'condition_title') ?></span>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <a href="/gallery/index?type=1" title="<?= Yii::t('common', 'gallery_title') ?>" alt="<?= Yii::t('common', 'gallery_title') ?>" class="main-service-link main-service-link-gallery">
                            <span class="main-service-link-title"><?= Yii::t('common', 'gallery_title') ?></span>
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <a href="#submit-application" title="<?= Yii::t('common', 'submit-application') ?>" alt="<?= Yii::t('common', 'submit-application') ?>" class="link-go-block main-service-link main-service-link-submit">
                            <span class="main-service-link-title"><?= Yii::t('common', 'submit-application') ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="submit-application" class="main-form-submit-application">
   <div class="main-form-application-content">
       <div class="text-center">
           <h2 class="text-heading"><?= Yii::t('common', 'title-application') ?></h2>
           <div class="main-block-form-link">
                <?=Html::a(Yii::t('common', 'list_participants'), Url::toRoute(['/site/participants']));?>
                <?=Html::a(Yii::t('common', 'condition_title'), Url::toRoute(['/site/condition']));?>
           </div>
       </div>

       <div class="main-form-application" id="registration">
           <div class="main-form-field-content text-center">
               <?php yii\widgets\Pjax::begin(['id' => 'note']) ?>
               <?php $form = ActiveForm::begin(
                       ['options' => ['enctype' => 'multipart/form-data',
                           'data-pjax' => true
                       ],
                           'id' => 'registration_id',
                       ]
               ); ?>

               <?= $form->field($registration, 'name')->textInput()->input('text', ['class' => "-state-empty"])->label(Yii::t('common', 'fio')) ?>

               <?= $form->field($registration, 'birthday')->textInput()->input('text', ['class' => "-state-empty"])->label(Yii::t('common', 'date_of_birth')) ?>

               <?= $form->field($registration, 'city')->textInput()->input('text', ['class' => "-state-empty"])->label(Yii::t('common', 'city')) ?>
               <?= $form->field($registration, 'study')->textInput()->input('text', ['class' => "-state-empty"])->label(Yii::t('common', 'place_study')) ?>
               <?= $form->field($registration, 'email')->textInput()->input('text', ['class' => "-state-empty"])->label('E-mail') ?>
               <?= $form->field($registration, 'phone')->textInput()->input('text', ['class' => "-state-empty"])->label(Yii::t('common', 'phone')) ?>


               <div class="form-attach-file-block">
               <?php
                    $msg = Yii::t('common', Yii::t('common', 'attach_file'));
                    echo $form->field($registration, 'file')->label(false,['class'=>'block-attach-file-size'])->widget(FileInput::classname(), [
                                    'options' => [],
                                    'pluginOptions' => [
                                        'showCaption' => false,
                                        'RTL' => true,
                                        'showRemove' => false,
                                        'showCancel' => false,
                                        'showUpload' => false,
                                        'showZoom' => true,
                                        'showLabel' => false,
                                        'preferIconicZoomPreview' => 'asda',
                                        'msgZoomTitle' => 'Детали',
                                        'dropZoneEnabled' => false,
                                        'elPreviewStatus' => '#customCaption',
                                        'elCaptionText' => '#customCaption1',
                                        'elPreviewContainer' => '#customCaption1',
                                        'browseClass' => 'genric-btn info-border more arrow',
                                        'browseRemove' => 'genric-btn info-border more arrow',
                                        'browseLabel' => "$msg",
                                        'browseIcon' => '<i style="font-size: 20px" class="glyphicon glyphicon-paperclip"></i>',

                                    ],

                    ]);
                    ?>
                   <div class="block-attach-file-size"><?php echo Yii::t('common', 'attach_file_size')?></div>
               <?=$form->field($registration, 'checkbox')->checkbox([ 'value' => '1', 'class' => '-state-empty'])->label(Yii::t('common', 'condition_accept'), ['class' => 'label-condition-accept']);?>
               </div>
                </div>
                </div>
                <div class="form-group-button text-center">
                    <?= Html::submitButton(Yii::t('common', 'send-application'), ['class' => 'genric-btn more danger', 'disabled' => 'disabled']) ?>
                </div>
           <?php ActiveForm::end(); ?>
           <?php \yii\widgets\Pjax::end()?>
       </div>
</section>

<?php
$this->registerJs(
'$("document").ready(function(){
$("#note").on("pjax:end", function(e) {
$("#registration_id").trigger("reset")
$(".main-form-field-content .form-control").addClass("-state-empty");
$(".main-form-application-content .form-group-button").removeClass("form-group-active-button");
$(".main-form-application-content .form-group-button .genric-btn").attr("disabled","disabled");
$("#registration-checkbox").addClass("-state-empty");
//document.getElementById("registration_id").reset();
$(\'#myModal\').modal();
}); 
});'
);
?>