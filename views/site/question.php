<?php
use yii\helpers\Html;

$this->title = Yii::t('common', 'TitleQuestions');
$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('common', 'descriptionQuestions')]);
$this->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('common', 'keywordsQuestions')]);
//$this->registerLinkTag(['rel' => 'canonical', 'href' => 'канонический урл']);

?>

<div class="page-image-block">
    <?= Html::img('/img/main/top-image-question.png', ['class' => "img-fluid"]); ?>
</div>


<div class="page-block-yellow-shape"></div>
<div class="page-block-blue-shape"></div>
<div class="container container-page">
    <div class="container-page-wrapper">
        <h1 class="text-left title-page-txt"><?= Yii::t('common', 'questions-and-answers') ?></h1>
        <p class="questions-text">
            <?= Yii::t('common', 'questions-text') ?>
        </p>
        <div class="block-fulltxt-page">
            <?php foreach ($content as $item){?>
                <b>
                    <?php echo $item->title;?>
                </b>
                <?php echo $item->text;
            } ?>
        </div>
    </div>
</div>