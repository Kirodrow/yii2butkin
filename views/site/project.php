<?php
/**
 * Created by PhpStorm.
 * User: Kirod
 * Date: 11.12.2018
 * Time: 19:45
 */

use yii\helpers\Html;
$this->title = $meta->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => "$meta->keywords"]);
$this->registerMetaTag(['name' => 'description', 'content' => $meta->description]);
?>

<div class="page-image-block">
    <?= Html::img('/img/main/top-image.png'); ?>
</div>


<div class="page-block-yellow-shape"></div>
<div class="page-block-blue-shape"></div>
<div class="container container-page">
    <div class="container-page-wrapper">
        <h1 class="text-left title-page-txt"><?= ucfirst($model->title) ?></h1>
        <div class="block-fulltxt-page">
            <?= $model->full_text ?>
            <?= Html::img('@web/img/main/project.png', ['class' => "img-fluid"]); ?>
        </div>
    </div>
</div>
