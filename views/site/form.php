<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<section  id="bottom" class="featured-area">

    <div class="container">
        <div class="row justify-content-center height align-items-center">
            <div class="col-lg-8">
                <div class="banner-content text-center">

                    <h3 class="text-heading">Заявка на участие</h3>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 "><a>Условия</a></div>
                            <div class="col-md-6 "><a>Список участников</a></div>
                        </div>
                    </div>


                    <?php $form = ActiveForm::begin(

                        ['options' =>['enctype' =>'multipart/form-data'],
                            'id' => 'registration_id',
                        ]
                    ); ?>

                    <?= $form->field($registration, 'name')->textInput()->input('text', ['placeholder' => "ФИО"])->label(false) ?>
                    <?= $form->field($registration, 'birthday')->textInput()->input('text', ['placeholder' => "День рождения"])->label(false) ?>
                    <?= $form->field($registration, 'city')->textInput()->input('text', ['placeholder' => "Город"])->label(false) ?>
                    <?= $form->field($registration, 'email')->textInput()->input('text', ['placeholder' => "E-mail"])->label(false) ?>
                    <?= $form->field($registration, 'study')->textInput()->input('text', ['placeholder' => "Место обучения"])->label(false) ?>
                    <?= $form->field($registration, 'phone')->textInput()->input('text', ['placeholder' => "Номер телефона"])->label(false) ?>
                    <?= $form->field($registration, 'file')->label(false)->fileInput( ['class' => "genric-btn info-border more "]) ?>
                    <?= $form->errorSummary($registration); ?>
                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'genric-btn more danger']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>


                </div>
            </div>
        </div>
    </div>
</section>