<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>

    <head>
    <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->

        <!-- Author Meta -->

        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

        <!--
        CSS
        ============================================= -->
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>

    <!--   -->
        <header>
            <div class="container">
                <div class="header-wrap">
                    <div class="header-top d-flex justify-content-between align-items-center">
                        <!--<div class="logo">
                            <a href="index.html"><img src="img/logo.png" alt=""></a>
                        </div>-->
                        <div class="main-menubar d-flex align-items-center">
                            <nav class="hide main-menubar-block">
                                <div class="menubar-language-block">
                                    <span class="footer-social">
                                        <a style="border-radius: 50%;" href="https://www.facebook.com/321startkz/"><i class="fa fa-facebook"></i></a><a style="border-radius: 50%;" href="https://vk.com/start321"><i class="fa fa-vk"></i></a><a style="border-radius: 50%;" href="https://www.youtube.com/channel/UCwLayHrpKfSFAKAwJs-FNYA/videos"><i class="fa fa-youtube-play"></i></a><a style="border-radius: 50%;" href="https://www.instagram.com/321_start/"><i class="fa fa-instagram"></i></a>
                                    </span>
                                    <?= $this->render('main/select') ?>
                                </div>
                                <div class="main-menubar-blocklink">
                                    <?=Html::a('<img class="main_logo" src="/img/main/321start-logo.png" title="">', Url::toRoute(['/site/index']), ['class'=>'logo']);?>
                                    <?=Html::a(Yii::t('common', 'about-project'), Url::toRoute(['/site/project']));?>
                                    <?=Html::a(Yii::t('common', 'condition_title'), Url::toRoute(['/site/condition']));?>
                                    <?=Html::a(Yii::t('common', 'participants'), Url::toRoute(['/site/participants']));?>
                                    <!--<?=Html::a(Yii::t('common', 'winners'), Url::toRoute(['/site/winners']));?>-->
                                    <a class="link-go-block" href="#submit-application"><?= Yii::t('common', 'apply-application')?></a>
                                    <?=Html::a(Yii::t('common', 'news-title'), Url::toRoute(['/gallery/index', 'type' => '0']));?>
                                    <?=Html::a(Yii::t('common', 'gallery_title'), Url::toRoute(['/gallery/index', 'type' => '1']));?>
                                    <?=Html::a(Yii::t('common', 'questions-and-answers'), Url::toRoute(['/site/question']));?>
                                    <a class="link-go-block" href="#block-contacts"><?= Yii::t('common', 'contacts-title')?></a>
                                </div>
                            </nav>
                            <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

    <div class="main-content-wrapper">
        <?=$content?>
        <!-- Start Feature Area -->


        <!-- End Service Area -->
        <!-- Start Amazing Works Area -->

            <footer>
                <div class="container">
                    <div class="footer_menu <?php if(Yii::$app->language == 'kz') echo "footer_menu_kz"; ?>">
                        <?=Html::a(Yii::t('common', 'main'), Url::toRoute(['/site/index']));?><?=Html::a(Yii::t('common', 'about-project'), Url::toRoute(['/site/project']));?>
                        <?=Html::a(Yii::t('common', 'condition_title'), Url::toRoute(['/site/condition']));?>
                        <?=Html::a(Yii::t('common', 'participants'), Url::toRoute(['/site/participants']));?>
                        <a class="link-go-block" href="#submit-application"><?= Yii::t('common', 'apply-application')?></a>
                        <?=Html::a(Yii::t('common', 'news-title'), Url::toRoute(['/gallery/index', 'type' => '0']));?>
                        <?=Html::a(Yii::t('common', 'gallery_title'), Url::toRoute(['/gallery/index', 'type' => '1']));?>
                        <?=Html::a(Yii::t('common', 'questions-and-answers'), Url::toRoute(['/site/question']));?>
                    </div>

                    <div class="footer-content d-flex justify-content-between align-items-center flex-wrap">
                        <div class="footer-phone-block" id="block-contacts">
                            <a class="footer-phone-link" target="_blank" rel="nofollow" href="tel:+7 (727) 250 60 96">+7 (727) 250 60 96 <span class="fa fa-phone"></span></a><a class="footer-phone-link" target="_blank" rel="nofollow" href="tel:+7 (727) 250 60 97">+7 (727) 250 60 97 <span class="fa fa-phone"></span></a>
                        </div>
                        <div class="footer-social-block">
                            <a target="_blank" rel="nofollow" href="mailto:hotline@cci.com.kz" class="footer-email-link">hotline@cci.com.kz</a>
                            <span class="footer-social">
                                <a style="border-radius: 50%;" href="https://www.facebook.com/321startkz/"><i class="fa fa-facebook"></i></a>
                                <a style="border-radius: 50%;" href="https://vk.com/start321"><i class="fa fa-vk"></i></a>
                                <a style="border-radius: 50%;" href="https://www.youtube.com/channel/UCwLayHrpKfSFAKAwJs-FNYA/videos"><i class="fa fa-youtube-play"></i></a>
                                <a style="border-radius: 50%;" href="https://www.instagram.com/321_start/"><i class="fa fa-instagram"></i></a>
                            </span>
                        </div>

                    </div>
                </div>
            </footer>
            <!-- End footer Area -->
    </div>

        <div class="modal fade" id="myModal">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                    <div class="modal-content text-center">
                        <div class="modal-content-wrapper">
                            <button type="button" class="close" data-dismiss="modal"><svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g filter="url(#filter0_d)">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M27 4.2L24.8 2L16 10.8L7.2 2L5 4.2L13.8 13L5 21.8L7.2 24L16 15.2L24.8 24L27 21.8L18.2 13L27 4.2Z" fill="white"/>
                                    </g>
                                    <defs>
                                        <filter id="filter0_d" x="0" y="0" width="32" height="32" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                                            <feOffset dy="3"/>
                                            <feGaussianBlur stdDeviation="2.5"/>
                                            <feColorMatrix type="matrix" values="0 0 0 0 0.929412 0 0 0 0 0.109804 0 0 0 0 0.109804 0 0 0 0.3 0"/>
                                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                                        </filter>
                                    </defs>
                                </svg>
                            </button>
                            <div class="modal-body">
                                <h2 class="text-heading text-center">Отлично!</h2>
                                <div class="text-center my-modal-text">Ваша заявка на участие принята. Ваша заявка на участие принята. Ваша заявка на участие принята.</div>
                            </div>
                            <button type="button" class="genric-btn more danger" data-dismiss="modal">Ок</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
