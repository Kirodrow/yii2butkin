<?php

use yii\bootstrap\Html;
?>
<div class="menubar-switch-language menubar-switch-language-<?php echo Yii::$app->language?>">
    <span class="customSwitches-ru">ru</span><input type="checkbox" class="custom-switch-input" id="customSwitches" <?php if(\Yii::$app->language == 'kz') echo "checked" ?>><label class="custom-switch-label" for="customSwitches"></label><span class="customSwitches-kz">kz</span>


<?php
    echo Html::a('Kz', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->controller->route, 'language' => 'kz']
    ), ['class'=>'customSwitches-link-kz']);
    echo Html::a('ru', array_merge(
        \Yii::$app->request->get(),
        [\Yii::$app->controller->route, 'language' => 'ru']
    ), ['class'=>'customSwitches-link-ru']);
?>
</div>
