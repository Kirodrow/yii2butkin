<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Registration */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="registration-view">

    <h1><?= Html::encode($this->title) ?></h1>



    <?

    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'birthday:ntext',
            'study:ntext',
            'city:ntext',
            'email:ntext',
            'phone:ntext',
            'date',
            $img,
            $download


            ]
    ]) ?>

</div>
