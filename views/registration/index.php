<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;
use app\widgets\LinkPager;
//use kartik\grid\GridView;
use kartik\export\ExportMenu;
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    'id',
    'name',
    'birthday',
    'study',
    'city',
    'email',
    'phone',
    'date',
];
/* @var $this yii\web\View */
/* @var $searchModel app\models\RegistrationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Зарегестрированные пользователи';
$this->params['breadcrumbs'][] = 'Список регистраций на сайте';
?>
<div class="registration-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'i18n'=>[
    'class' => 'yii\i18n\PhpMessageSource',
    'basePath' => '@kvexport/messages',
    'forceTranslation' => true
            ],
        'dropdownOptions' => [
            'label' => 'Export All',
            'class' => 'btn btn-secondary'
        ]
    ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=>"{summary}\n{items}",


        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'birthday',
            'study',
            'city',
            'email',
            'phone',
            'date',
            'file',
            [
                'class' => 'yii\grid\ActionColumn',

                'headerOptions' => ['width' => '80'],
                'template' => '{view}  {link}',
            ],


        ],
    ]); ?>

    <div class="col-md-12 ">
        <div class="row margin20 ">

            <div class="pagination_gallery">

                <?php echo \app\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->pagination,

                    'pageCssClass' => 'pageClass',
                    'activePageCssClass' => 'pageClass currentPage',
                    'maxButtonCount' => 7,
                    'prevPageLabel' => '&lsaquo;',

                    'nextPageLabel' => '&rsaquo;',
                    'prevPageCssClass' => 'prev',
                    'nextPageCssClass' => 'next',

                    'activePageAsLink' => false,
                    'disableCurrentPageButton' => true,
                    'hideOnSinglePage' => true,
                ]); ?>
            </div>
        </div>
    </div>

</div>
