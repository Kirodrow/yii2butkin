<?php

use yii2mod\rbac\migrations\Migration;

class m181210_040645_create_role_admin extends Migration
{
    public function safeUp()
    {
        $this->createRole('admin', 'admin has all available permissions.');

    }

    public function safeDown()
    {
        echo "m181210_040645_create_role_admin cannot be reverted.\n";

        return false;
    }
}