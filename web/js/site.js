$(document).ready(function () {
    mainStart.init();
});

$(window).bind('resize',function () {
    mainStart.heightMainBanner();
});



let mainStart = (function () {
   const mainBanner = '.banner-area-main';
   const scrollElement = '.link-go-block';
   const linkMoreRead = '.link-show-maintxt';

    return {
        init: function () {
            this.initscrollToBlock();
            this.heightMainBanner();
            this.scrollToBlock();
            this.moreReadTxt();
            this.transferLabel();
            this.initFormControls();
            this.checkLanguage();
            this.initVideo();
        },

        heightMainBanner: function () {
            $(mainBanner).find('.container').height($(window).height());
            $(mainBanner).find('.banner__video').height($(window).height());
        },

        scrollToBlock: function () {
            $(scrollElement).on('click', function (e) {
               let urlLink = $(this).attr('href');
               let menubar = $('.main-menubar-block');

               if(!(menubar.hasClass('hide'))) {
                   $('.menu-bar .lnr-cross').click()
               }

                if($(urlLink).length) {
                    $('html, body').stop().animate({
                        scrollTop: $(urlLink).offset().top
                    }, 800);
                    e.preventDefault();
                }

                else {
                    location.replace('/'+urlLink);
                }

            });
        },

        initscrollToBlock: function () {
            let tabs_name=window.location.href.split('#')[1];

            if(typeof(tabs_name) !="undefined") {
                history.pushState("",document.title, window.location.pathname);
                //$('html, body').stop().animate({scrollTop: $('#'+tabs_name).offset().top}, 800);
            }
        },

        transferLabel: function () {
            let myformField=$('.main-form-field-content .form-group');
            let labelCondition=$('.label-condition-accept input');

            $(myformField).each(function() {
                let Label= $(this).find('.control-label');
                let Input= $(this).find('.form-control');
                Label.remove();
                $(Label).clone().insertAfter(Input);
                mainStart.checkInputState(Input);
            });

            $(labelCondition).each(function() {
                $(this).clone().insertBefore($(this).parent());
                $(this).remove();
            });

            if($('#registration-phone').length) $('#registration-phone').mask('+77 (999) 999 99 99');
            if($('#registration-birthday').length) $('#registration-birthday').mask('99.99.99');;

        },

        initFormControls: function () {
            $('.main-form-field-content .form-control').keyup(function(){
                let el = $(this);
                mainStart.checkInputState(el);
            }).change(function(){
                let el = $(this);
                mainStart.checkInputState(el);
                mainStart.checkViewButton();
            });

            $('.main-form-field-content #registration-checkbox').change(function(){
                let input = $(this);
                input.toggleClass('-state-empty');
                mainStart.checkViewButton();
            });
        },

        checkLanguage: function() {
            $('#customSwitches').change(function(){
                let input = $(this);
                input.parents('.menubar-switch-language').removeClass('menubar-switch-language-ru').removeClass('menubar-switch-language-kz');

                if($(input).is(':checked'))  {
                    input.parents('.menubar-switch-language').addClass('menubar-switch-language-kz');
                    $('.customSwitches-link-kz')[0].click();
                } else {
                    input.parents('.menubar-switch-language').addClass('menubar-switch-language-ru');
                    $('.customSwitches-link-ru')[0].click();
                }
            });
        },

        checkInputState: function(el) {
            if(el.val()==''){
                el.addClass('-state-empty');
            }else{
                el.removeClass('-state-empty');
            }
        },

        checkViewButton: function() {
            let myForm= $('.main-form-application-content');
            let countEmptyField=$(myForm).find('.-state-empty').length;
            let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

            if((countEmptyField == 0) && (pattern.test($('#registration-email').val()))){
                $(myForm).find('.form-group-button').addClass('form-group-active-button').find('.genric-btn').removeAttr('disabled');
            }

            else {
                $(myForm).find('.form-group-button').removeClass('form-group-active-button').find('.genric-btn').attr('disabled');
            }
        },

        moreReadTxt: function () {
            $(linkMoreRead).on('click', function (e) {
                let txtShort = $(this).parents('.sample-text').find('.short-text-block');
                let txtFull = $(this).parents('.sample-text').find('.full-text-block');
                /*$(this).prev().remove();
                $(this).remove();
                $(txtBlock).animate({height: 'show'},500);*/
                /**/
                $(txtFull).show();
                $(txtShort).hide();
                e.preventDefault();
            });
        },

        initVideo: function () {
            let video = $("#banner-video");
            if (video.length){
                video.on("canplay", function(){
                    setTimeout(function(){
                        video.animate({
                            opacity: 1
                        }, 1000);
                    }, 2000);
                });
            }
        }
    };
}());



function deleteImage(id) {
    $.ajax({
        type: 'post',
        data:{id: id},
        url: '/images/del',
        success: function(data) {
            $('#img'+id).hide();
        }
    });
}

function  changeSelect() {
     val = $('#type').val();
    if(val == 0){
        $('#gallery').show();
    }
    else {
        $('#gallery').hide();

    }
}


