<?php

use yii\db\Migration;

/**
 * Class m190104_090447_create_description
 */
class m190104_090447_create_description extends Migration
{
    /**
     * {@inheritdoc}
     */

        public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('meta', [
            'id' => $this->primaryKey(),
            'type' => $this->string(32)->notNull(),// Название статьи
            'news_id' => $this->string(32),// Название статьи
            'title' => $this->text(), //Краткий текст
            'keywords' => $this->text(), //Полный текст
            'description' => $this->text(), //Изображение в списке
             ], $tableOptions);
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190104_090447_create_description cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190104_090447_create_description cannot be reverted.\n";

        return false;
    }
    */
}
