<?php

use yii\db\Migration;

/**
 * Handles the creation of table `registration`.
 */
class m181209_000113_create_registration_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('registration', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'birthday' => $this->text()->notNull(),
            'study' => $this->text(),
            'city' => $this->text(),
            'email' => $this->text(),
            'phone' => $this->text(),
            'date' => $this->integer()->notNull(),
            'file' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('registration');
    }
}
