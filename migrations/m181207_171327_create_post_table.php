<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m181207_171327_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(32)->notNull(),// Название статьи
            'gallery' => $this->integer(), //галерея для новости
            'full_text' => $this->text(), //Полный текст
            'short_img' => $this->text(), //Изображение в списке
            'date' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(), //Номер категории
            'city' => $this->string(),
            'activity' => $this->integer()->notNull()->defaultValue(0), // Активность статьи
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
