<?php

use yii\db\Migration;

/**
 * Class m190103_133523_create
 */
class m190103_133523_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('main', [
            'id' => $this->primaryKey(),
            'img' => $this->string(64),// Название статьи
            'video' => $this->text(), //Краткий текст

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190103_133523_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190103_133523_create cannot be reverted.\n";

        return false;
    }
    */
}
